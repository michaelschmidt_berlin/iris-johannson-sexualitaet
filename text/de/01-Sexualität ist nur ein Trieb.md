## Sexualität ist nur ein Trieb

Du hast mich gebeten, dir zu erklären und auch deiner Freundin zu erklären, die heute mit dir gekommen ist, warum ich sage, dass Erotik und Sexualität nicht Liebe sind, sondern ein Trieb sind, ein Werkzeug zum Überleben der Menschheit und gerade ein Trieb _sind_, der sich einschaltet, wenn wir jemanden begegnen, der oder die ein potentieller Partner oder eine potentielle Partnerin sein kann.
Und dass die Erotik in allen möglichen und unmöglichen Situationen sich einstellen kann.
Und ich sage, dass die Natur trotzdem ziemlich lieb ist und die Partner zurecht schneidet, die auch zu uns passen können nachdem die Verliebtheit vorbei gegangen ist und nachdem die Kinder von zu Hause weggezogen sind.
Das Lust ein etwas instabiler Grund ist eine lebenslängliche Beziehung aufzubauen, weil sie hinterhältig kommen und gehen kann, ein bisschen wie es ihr beliebt, und mit uns ihr Spiel treiben kann wie wie Reflektionen des Sonnenlichtes, die an eine Wand Spiegel und uns manchmal blenden.

<div class="handwritten">
Du bist so krass und hart wenn du so sagst.
Wir andere, wir wollen etwas Romantik haben, und wenn du das so sagst dann scheint es so als wäre es nur ein tierischer Trieb und ich finde nicht, dass das stimmt.
</div>

Du hast recht es kann viel mehr als das sein.
Und es kann alle anderen Gefühle, die du fühlst auch beinhalten und auch die Gefühle beinhalten, die für dich zentral sind.
Aber warum ich dies so sage ist, weil die meisten das überspringen, sich damit auseinanderzusetzen, dass es in der Tat ein Trieb ist, der im Dienst der Menschheit steht, zum Überleben der Art.
Viele erheben das zu Geistigem, Heiligem, und wollen dass zu etwas Göttlichem machen, und das ist das nicht.
Schon, wir können irgendetwas erhöhen, hinauf in eine in eine Metaebene, die man nie beweisen kann.
Erst wenn wir gestorben sind bekommen wir die Antwort, ob wir zu Gott gelangen oder nicht, wenn es einen Gott gibt, zu dem man kommen kann.

Aber wir haben alle jedes Recht an Gott zu glauben und ihn für wahr zu halten und dass _er_ heilig ist.
Die Unreinheit im Becher ist, wenn du erlebst die Sinnlichkeit, das Vergnügen, den Genuss, wonnigliche Seligkeit und so weiter in der Wirklichkeit erlebst, in der Beziehung mit dem du die Erotik erlebst, dann, nach heiligen Büchern, die von Menschen geschrieben sind, da enttäuscht du Gott.
Es ist nur in Beziehung zu Gott, in der dir erlaubt ist, das große Glück zu erleben, Agape.
Mit einem anderen Menschen darfst du nur das niedrigere Eros erleben und nur wenn du vorhast, Kinder zu zeugen, ansonsten gilt Enthaltsamkeit.
Denn unterbrochener Geschlechtsverkehr ist Selbstbefleckung und eine Todesünde.

Und es ist ja auch so, dass wenn wir in der Nähe eines Menschen leben, so gibt es da eine Überschneidung.
Die Atmosphäre des einen färbt die Atmosphäre des anderen ein und umgekehrt.
Und deswegen fangen wir an, Eigenschaften beim anderen zu schätzen, mit denen wir vorher nicht vertraut gewesen sind.
Das führt manchmal zu einem Zusammenklingen und dazu, dass wir lieber zum Gefühl heranziehen in uns, in dieser Berührung des anderen.
Und das kann während der Jahre wachsen.
Früher, als die Leute sich nicht so viel bewegt haben, so haben sie oft ihren Partner innerhalb eines Diameters von 8 km gefunden, und oft waren sie da Schulkameraden und haben die Familien voneinander gekannt, hatten die gleichen Traditionen, und da funktionierte das Zusammenklingen schon von Anfang an.

Natürlich kann es in die andere Richtung auch gehen.
Das was man am Anfang so bezaubernd fand wird nach einer Weile als äußerst unangenehm empfunden und störend und schafft Abstand, anstatt dass wir uns einander annähern.
Und da wird es oft so erlebt wie wenn die Liebe ausgeht.
Was sie nicht tun kann, aber man hört auf, sie hochzuziehen, sie in einem selber heranzuziehen, und man baut stattdessen negative Bewertungen an.

