## Mein Samenerguss kommt immer zu schnell. Und dann möchte ich nicht mehr.

<div class="handwritten">
Nächste Frage.
Ein Mann sagt „Mein Samenerguss kommt immer zu schnell.
Er kommt fast direkt.
Und dann ist es bei mir vorbei, dann möchte ich nicht mehr.
Und das, findet meine Frau, ist sehr traurig.
Und das verstehe ich.
Was soll ich tun?“
</div>

Hat es angefangen mit deiner Geschlechtsreife oder ist es später im Leben so gekommen?
Hast du Angst, dass du nicht „fertig werden sollst“ oder hast du eine Vorstellung davon, dass du „so schnell wie möglich fertig werden sollst“.
Ich kann mir vorstellen, dass es daran liegt, dass du den Fokus auf die Abrundung hast.
Und dass es dies ist, was der Höhepunkt ist, was man gewiss erreichen soll, ansonsten ist es irgendwie schlecht.
So ist es nicht.
Der Samenerguss ist nur das Abschließen.
Und das ist nicht, was das Schöne beim Ganzen ist.
Es kann eine solche große Versuchung sein, den Fokus darauf gerichtet zu haben, anstelle davon, den Fokus beim Partner zu haben, und das was im Sein, in dem Empfindungsfeld voneinander geschieht.
Aber konzentriere dich darauf, so verändert sich vielleicht etwas.
Nimm dir erstmal Zeit, viel Zeit anwesend zu sein, und den Sog im Körper zu genießen.
Die erotischen Gefühlen, die es sind.
Die Gedanken und die Bilder, die in dir geschaffen werden, die dich dazu bringen zu schaudern.
Und du spürst eine starke Anziehung und Lebenskraft.
Das ist ein wichtiger Teil der Sexualität.
Dann alles Kuscheln und Streichlen und Daherreden, und Albernheit und Spielen, was dem Ganzen einen Schimmer verleiht.
Danach kommt der Beischlaf an sich.
Das heißt, dass ihr euch ineinander verschlingt, in ein Dadrunter oder Dadrüber.
Oberhalb, aneinander herum, auf 1000 verschiedene Weisen.
Und auf eine Weise, die euch recht ist.
Dass ihr innehaltet, jedesmal wenn du spürst: jetzt Pause.
Verbleibe, schließe die Augen, such in dir selber und halte inne, bis das Anheizen zum Erguss vorbeizieht.
Mach so weiter bis ihr euch reif fühlt, eines gemeinsamen Abschlusses.

Wenn der Samenerguss kommen sollte bevor ihr beide soweit seid, so macht weiter mit dem Spielen, mit den Albernheiten.
Ihr könnt liebevoll weitermachen, es wird in in irgendeiner Hinsicht wiederkehren, in ausreichendem Ausmaß, und streichelt euch und spielt.
Du wirst die Sexualität auf eine ganz neue Weise entdecken, wenn du nicht den Fokus auf den Abschluss, auf den Samenerguss hast, sondern auf dem Sein an sich in der Erotik.

