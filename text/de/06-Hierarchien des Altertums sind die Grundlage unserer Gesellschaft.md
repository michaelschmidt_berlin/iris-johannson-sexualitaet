## Hierarchien des Altertums sind die Grundlage unserer Gesellschaft

Aber das Letzte, worüber du geredet hast das, was mit den Religionen heutzutage zu tun hat, davon hätte ich gerne, dass du weitersprichst.

In dem Griechenland des Altertums, dass in der Zeit die Kultur hatte, die am höchststehenden war, da gab es Gottheiten die Ideale waren und denen man vertraute.
Von ihnen wurde viel erzählt, zum Beispiel von Zeus, der der größte Gott war, der Gott über allen anderen Göttern.
Und er hat eine Tochter durch sein Haupt geboren und sie war die größte Göttin.
Sie waren erhöht über dem Menschlichen.
Und das Menschengeschlecht war ihnen keine Bedrohung.
Und die Menschen vertrauten darauf, dass die Götter walteten zum Wohle der Menschen.

Dann gab es Götter, die herunter stiegen.
Und es gab Frauen, die befruchtet durch die Götter wurden und so wurden Halbgötter geboren.
Sie waren immateriellerweise unsterblich, aber sie waren in Menschenkörpern.
So wurde der Gedanke von himmlischen Wesen geboren.
Diese wurden die Herrscherklasse auf auf der Erde.
Sie standen höher als normale Menschen und deshalb sollten normale Menschen ihnen gehorchen.
Das war der Anfang der Klassengesellschaft, unter der wir immer noch leben, obwohl wir versuchen sie aufzulösen.
Und das liegt daran, dass sie nicht unterscheiden eine natürliche primäre Rangordnung von einer erschaffenen Klasse, einer Hierarchie.

Das menschliche System hatte verschiedene Stände.
Herrscher, die Aristokraten waren Krieger, die Kriege waren ihr Hirt, Philosophen waren Senatoren und Politiker und Bürger in dem Weltlichen und bereicherten das Äußere mit ihrer Großartigkeit.
Und in dem war die Sexualität nicht reserviert.
Sie war gar kein Thema.
Sie wurde reguliert durch Möglichkeiten.
Und sie verursachte eine ganze Menge Eifersuchtsdramen.
Bei dem standen die Männer gegen die Männer, denn die Frauen waren ausgespart dieses Kampfes.
Und dadurch war es eine Form der Ebenbürtigkeit, auch wenn es die Männer waren, die über das Weltliche entschieden.

Der Rest der Menschen bei dieser Gesellschaft waren Sklaven.
Sie stammten aus anderen Teilen des Landes und der Welt.
Und man schaute auf sie als auf Halbmenschen.
Und es gab sie, um alle Notwendigkeitsarbeit, die Drecksarbeit zu machen.
Und je zahlreicher sie waren, desto besser.
Und am liebsten sollten sie unbegrenzt Kinder kriegen.
Dadurch wurden die privilegierten noch reicher, sie hatten es noch bequemer dadurch.
Für die Sklaven galten ganz andere Gesetze.
Aber einige konnten durch ihr Handwerk und durch ihre Kapazität, ihre Freiheit erkaufen.
Und den Niedrigststehenden angehören.
Aber sie hatten auch Privilegien und es waren diese die den Grund der Bürgerklasse legten.
