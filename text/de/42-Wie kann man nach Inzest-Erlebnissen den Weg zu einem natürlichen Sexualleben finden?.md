## Wie kann man nach Inzest-Erlebnissen den Weg zu einem natürlichen Sexualleben finden?

<div class="handwritten">
Nächste Frage.
Hier kommen einige Fragen, die davon handeln, seinem Trieb ausgeliefert zu sein.
Kannst du sie beantworten auf eine Weise, die eine konkrete Hilfe sein kann?
Wenn man dieses ganze Heftchen gelesen hat, dann versteht man vielleicht diese Fragen selber auf eine tiefere Weise und kann die Ratschläge annehmen, die du anbietest, damit die Personen aus ihren eigenen, unbeweglichen Positionen rausgehen.
Ist das möglich?
</div>

Ich kann meistens etwas sagen, was ein Schlüssel sein kann, mit dem man weiter suchen kann.
Vielleicht so, dass man eigene eine eigene Frage formulieren kann, die die Person nicht früher gedacht hat, und so weiter.

Wenn man Inzest erlebt hat und keine Sexlust spüren kann, weil die Erwachsenen ekelig sind in den eigenen Augen, kann man durch Zuhören Hilfe bekommen.
Ein Abonnement zu haben, damit man anrufen kann, wenn man es braucht.
Den Ekel zu entladen, kotzen darüber und schreien, brüllen, vertäufeln, weinen und so weiter.
All das, was noch da ist nach dem Übergriff, und all das, was um einem als man klein war geschehen ist.

Jemanden, der oder die eine Atmosphäre hat, so dass man in Kontakt mit jemandem sein kann über das Telefon, mit einem unbekannten Menschen, der sonst nicht mit dem eigenen Leben zu tun hat, das ist der beste Weg, Abstand zu den Inzestübergriffen zu bekommen.
Ich kann so einen Kontakt vermitteln, wenn es nötig ist.

<div class="handwritten">
Wenn die Lust verschwindet, weil man als Kind Gewalt und Übergriffen ausgesetzt war.
</div>

Schaue das geradeeben Gesagte an:
Man braucht da die gleiche Hilfe, als eine Ergänzung der Bearbeitung der Gelegenheiten, die schockiert haben, was Auslöser des Ganzen war und das, was dann zu posttraumatischem Stress geworden ist.
Und man braucht das, was ich Primärarbeit nenne, um heranzuwachsen innen zum Erwachsenen.

Das Schwierige an Inzest ist, dass es oft die Person ist, von der man die meiste Liebe spürt, der oder die dann einen sexuell ausnützt.
Und das reimt sich nicht.
Kinder fühlen, dass es Unrecht ist, aber sie verstehen nicht, was falsch ist.
Und tun, was der Erwachsene möchte, weil Kinder meinen, dass es so ist wie es sein soll.
Deshalb wird es so schwer, wenn man selber geschlechtsreif wird und erotische Empfindungen hat, weil sie gehören quasi nicht zusammen mit einem selber, sondern nur mit dem anderen, und da entsteht oft anstelle dessen ein Ekel.
Der Mensch, der diesem ausgesetzt wurde, braucht jemanden, mit dem er reden kann, oder eher jemanden, der ihm zuhört, wenn er erzählt von all den doppelten Gefühlen, wenn er reagiert und entlädt.
Er bringt sie hierher und jetzt und lebt sie durch, in Anknüpfung an die erwachsene Vernunft.
Bis sie das Inzestuöse zur Geschichte werden lassen können.
Und sie dann nicht mehr einwirken zu lassen aktiv, wenn es um Beziehungen der Gegenwart geht.
