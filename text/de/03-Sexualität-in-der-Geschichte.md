## Sexualität in der Geschichte

<div class="handwritten">
Ja, ich folge dir soweit, aber es fällt mir denn noch schwer zu glauben, dass eine Beziehung genauso gut sein kann, wenn sie aus den Strategien andere Menschen her kommt, weil es jemandem recht ist so.
Ich denke an alle Könige und andere hochbürtige Leute.
Die hatten ja eine Unmenge an Kindern einfach drumherum, weil sie gezwungen waren zu heiraten nach Traditionen und Vereinbarungen.
Und, ja, auch weil der Friede zwischen Ländern aufrechterhalten werden musste.
</div>

Ja, das stimmt, aber dann musst du wissen, dass ist eher ein Beweis für die Unabhängigkeit des Triebs in Beziehung zu der Vernunft ist, alles umgekehrt.
Diese Aristokraten, sowohl Männer als auch Frauen, wurden auf eine Weise erzogen, bei der es als primitiv angesehen wurde, mehr attrahiert zu sein als man es war, wenn man Damen den Hof gemacht habt, wenn es ein Fest gab.
Ansonsten waren Damen nur etwas, was man im Bett hatte und was die eigenen Kinder gebar.
Und die Frauen haben dann die Kinder in der Weise erzogen, in der gleichen Weise zu denken, in der Weise, die passte in diesen Kreisen.
Diese Anpassung führte dazu, dass man eine eine tiefe Verachtung gegenüber sich selbst selber empfand.
Dass man überhaupt es über sich ergehen ließ, sich auf diese Weise anzupassen.
Und dann bekam der Trieb ein Quell der Freiheit.
Gerade dies, sich vergnügen und ein Genuss verschaffen zu können, aus der Eigenaktivität heraus.
Und dass man der frommen Tradition trotzen konnte, Geliebte sozusagen seitlich des Lebens zu haben.
Und die Frauen hatten Liebhaber, die durch die die Geheimtür kamen zu ihrem Gemach.

Es gab immer arme Frauen, die ihre Körper verkauft haben.
Und es gab immer arme, hübsche Jünglinge, die das gleiche taten, und die kompensiert dafür wurden.
Oft war die Situation der Frauen existenziell.
Sie mussten dies tun, um zu überleben, und dabei war ihnen wohl bewusst, dass sie auf der Straße landen konnten, wenn sie nicht mehr verzauberten und bezauberten denjenigen, der ihnen hold war.

Die Schönheit erlischt mit der Zeit und da wurden sie gegen schönere, jüngere Objekte ausgetauscht.
Es sei denn, es gelang ihnen ihren Partner in eine symbiotische Beziehung hineinzuverwickeln, sodass der Partner es nicht aushalten konnte frei zu sein.
Im Falle der jungen, armen Männern, schönen Männern, so ging es immer darum, dass sie sich in den Salons der der wohlhabenden bewegten, um diese wohlhabenden Damen zu sehen, seien sie verheiratet oder unverheiratet, und Beziehung mit ihnen aufzunehmen.
Die Frauen hatten immer ihre eigenen Zimmer und meistens gab es Geheimgänge.
Man war der Ansicht, dass die Frauen die Möglichkeit haben sollten, Bedienung aus der Küche heraus bekommen zu können, wenn sie nicht angezogen waren im Bett.
Frühstück zum Beispiel, ein Bad zu bekommen und anderes.
Aber oft wurden sie fleißig benutzt durch Kavaliere, die den Hof den Damen machten.
Es war ein öffentliches Geheimnis, von dem man nicht laut sprach.
Die Kirche verurteilte dies, aber oft waren es die Männer der Kirche, die diese Liebhaber darstellten.

Die verheirateten Frauen hatten nicht so große Schwierigkeiten, denn niemand kümmerte sich darum, wer der Vater ihrer Kinder war.
Es war immer der Mann, der der Vater war, wer auch immer die Frau befruchtet hatte.
Schlimmer war es für eine Jungfrau.
Sie musste schnell schauen, dass ihre Eltern sie wegvermählten und wegverheirateten, bevor man sehen konnte, dass sie schwanger war.
Aber wer dann heiratete, hat seine Gewohnheiten mit anderen Frauen nicht geändert.
Und so kümmerte er sich selten darum, dass sie schwanger war.
Und außerdem hatten die Männer es gern, dass die Frau sexuelle Erfahrung hatte und es verstand wie sie tun sollte, um einen Mann im Bett zufriedenzustellen.
Dies galt für die oberen Stände.
Aber es verbreitete sich zu den Bürgerfamilien auch, die ihren Status so zeigte, indem sie Diener hatten, und ein öffentliches Leben hatten und dann ein anderes privaterweise hatten.

Außerhalb dieses Systems waren die Bauern und die Armen.
Sie lebten auf eine ganz andere Weise.
Oft wohngemeinschaftlich, eng zusammen, damit sie füreinander da waren, die Wärme aufrechtzuerhalten im Winter.
Oft waren sie kurz gehalten durch die Oberklasse und die Bürgerklasse, damit sie kaum überlebten.
Und bei ihnen schaute man so darauf, dass die Männer alle Frauen versorgen sollten.
Und sie bekamen eine Art des Lohnes für ihre Arbeit in Naturalien, in Essen und Unterkunft.
Und auch eine kleine Geldsumme für den Rest, was sie brauchten.

Die Frauen bekamen nichts mehr als dass sie in der Tat wohnen durften bei ihren Männern, und ihre Arbeit war war inbegriffen im Lohn des Mannes.
In der Unterklasse war die Sexualität reserviert für das Paar und dies aus praktischen Gründen.
Ihnen fehlte der Luxus, sich Gefühlsspielen und dem Kurtisieren sich hingeben zu können.
Sex war ein Vergnügen und ein Genuss, war eine Weile sich Entspannens inmitten des harten Daseins am Rande der Existenz.
Außerdem wollte die Oberklasse, dass die Unterklasse so viele Kinder wie möglich bekommen sollte, damit die die Arbeitstruppe nicht abnahm, wenn Erwachsene früh wegstarben.
Und deshalb wurde es das Los der Frauen, Kinder zu kriegen, und mit dem, was da war, zu haushalten, wenn überhaupt etwas zum Haushalten da war.
Die einzigen Verhütungsmittel, die es gab, waren, so lange wie nur möglich zu stillen und am liebsten viele und am liebsten viele Kinder gleichzeitig zu stillen, denn da sprangen keine neuen Eier, da konnte man frei sein des Kindergebärens, bis zu vier Jahren.
Die zweite Methode war unterbrochener Beischlaf.
Das war der Religion nach Sünde, aber es kam niemand, der das kontrollieren konnte.
Davon wurde dann trotzdem Gebrauch gemacht.

Wenn ein Paar eine schönes beiderseitiges Verhältnis hatten, so war das oft so, dass der Mann es tat um seiner Frau Willen.
Und das führte oft dazu, dass die Frau nicht mehr als drei oder vier Kinder bekam.
Im Gegensatz dazu sieben bis zehn, wie es üblich war.
