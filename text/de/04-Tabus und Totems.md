## Tabus und Totems

<div class="handwritten">
Was ich dabei bemerke, wenn du das sagst ist, dass die Sexualität überall ist und in allen Situationen.
Und sie wird eingekreist auf verschiedene Weise.
Sie gibt es nie frei fließend als der Trieb, den er zu sein scheint.
Ich frage mich, wie er an sich funktionieren sollte.
</div>

In den meisten Ursprung-Kulturen gibt es Totem und Tabu.
Das heißt, dass es das etwas da ist was, Leben schützen soll.
Und es gibt Verbote gegen manche Sachen, die schädlich sind.
Um sie zu verstehen braucht man vertraut zu sein mit den Sitten und Gebräuchen eines Stammes.
Wie er organisiert ist, wie er geführt wird und welche Mittel es gibt um den Stamm zu lenken.
Ich habe vor, ein Beispiel anzusprechen einer solchen Kultur.

Aber vergiss nicht, dass jede Herde, jedes Schar ihre eigenen Totems und Tabus hat.
Und deshalb ist es nicht möglich zu verallgemeinern.
Man kann es nur als eine von vielen Vorstellungen haben.
Eine Herde bildete sich heran auf einen Platz, wo es Voraussetzungen gab zum Leben.
Das heißt, dass Essen da war, dass Ernährung da war für die bleibende Herde.
Die Gruppe erwählte einen Dorfältesten, einen Medizinmann, Schaman oder sowas.
Das ist jemand, der den Ausschlag gibt bei einer Streitigkeit.
Der übliche Weg zu Entscheidungen war Konsensus.
Wenn niemand dagegen war, da konnte man es durchführen.
Aber wenn das nicht möglich war, dann fanden Verhandlungen über eine gewisse Zeit statt, und wenn es trotzdem nicht möglich war, zum Entschluss zu kommen, sogar bis jemand, der klug war und der erwählt worden war, der die Entscheidung treffen sollte.

Die Männer machten oft eine Schar aus die kam und ging.
Sie jagten größere Beutetiere, wo sie miteinander zusammenwirken brauchten, um es um das Beutetier erlegen zu können.
Gleichzeitig mussten sie miteinander zusammenwirken, um schlafen zu können, und um in der wilden Natur klarzukommen.
Ein Rest von dieser Männer-Herden-Mentalität gibt es noch beim Sport.
Zum Beispiel, eine Fußballmannschaft ist ein typisches solches Überbleibsel.
Dann wanderten sie vom Dorf weg und machten den Weg zum nächsten Dorf und da entstand Feier und Freude.
Sie brachten gutes Essen.
Und diese Gäste wurden verehrt mit der Gunst der Frauen.
Die Leute im Dorf, die Frauen, die Kinder, die Gebrechlichen und die nicht arbeiten konnten, die arrangierten ein Feuer auf dem Platz mitten im Dorf.
Und dieses Feuer brannte und es hielt die wilden Tiere weg.
Die Leute tanzten um dieses Feuer herum.
Sie stampfen und sie taten Laute, um gefährliche kleine Tiere fernzuhalten.
Die Schlangen, die Skorpione, die Spinnen und andere Tiere.
Es entstand ein Geborgenheitsgebiet.
Kinder, Alte und diejenigen, die nicht arbeiten konnten, zogen sich zurück zu der Schlafscheune.
Und die Männer und die Frauen fuhren fort zu singen, zu tanzen, zu essen.
Und diese äußere Geborgenheit machte es möglich, in der inneren geborgenen Trance zu sein.

Da gedeihte die Erotik.
Und die Sexualität kam auf und wurde geübt die Tage, wo dies vonstatten ging.
Es war der Vermehrungsakt.
In dem gab es nicht die Zweisamkeit.
Denn es war nicht sicher, dass diese Männer wiederkehren würden.
Die Teilnehmer taten diese Orgie und neun Monate später wurde eine Anzahl von Kindern geboren.
Da die Mama sich um das Kind kümmerte, so wusste man von wem es war.
Und wenn sie starb, so gehörte das Kind dem Dorf und jemand anders sorgte sich um es.

Die Männer verließen danach das Dorf und zogen weiter.
Und vielleicht gelangten sie in ein anderes Dorf und wiederholten das Ritual.
Wenn diese Gruppe Männer kam, hatten sie lange nichts zum Essen gehabt.
Und sie hatten es nötig, essen zu dürfen, schlafen zu dürfen, und auch dass für sie gesorgt wurde.
Da war es wichtig, dass keine Kinder da waren und keine alten Leute in der Nähe waren.
Denn es war tabu für sie, daran teilzunehmen.
Sie hielten sich fern, damit keine Lüsternheit auf Kinder und nicht geschlechtsreife Kinder entstehen würde.
Ein Grund war auch, dass diese Männer Kinder schlachten und aufessen konnten.
Denn sie hatten keine Beziehung zu ihnen.

Wer mit diesem Verbot brach, wurde hinausgeworfen, hinaus in die wilde Natur.
Und das war gleichbedeutend mit dem Tod, denn es war ein Tabu, dass man einfach nicht brechen durfte.
Diese Männer,  die die Herde verließen, wurde ein Talisman mitgegeben.
Etwas, woran sie sich halten konnten, um die Schärfe ihre Sinne nicht zu verlieren.
Damit die Angst sie nicht blind machte, um sie in ihrer eigenen Geborgenheit behalten zu können.
Wohin sie zogen, wusste niemand.
Oft gab es einen TotemPfahl.
Er war dazu da, mit allen bösen Kräften klarzukommen, die diese Zamonien umgaben.
Und er sollte vor gewaltsamem und direktem Tod bei den Kupplungsgelegenheiten schützen.

Wenn diese Männer wegzogen, dann haben sie die Jungs mit, die gerade dabei waren, geschlechtsreif zu werden.
Und sie durften ein Teil der Gruppe sein.
Und sie durften vertraut werden mit den männlichen Ritualen.
Die Gruppe ließ auch hinter sich einige, die die Gruppe nicht mehr brauchte.
Sie durften bleiben.
Es waren die, die älter waren und die nicht mehr die Kraft hatten, aktiv beim Jagen zu sein.
Und sie wurden gebraucht im Dorf, für die Herde, um Zäune um die Wohnhäuser zu bauen, oder um Häuser zu bauen zum Wohnen, unter anderem.
Ihr Beitrag war im Ganzen sehr wichtig.
Bei manchen Stämmen durften diese Männer bei verschiedenen Frauen schlafen.
Und sie durften so ihre sexuelle Fähigkeit aufrecht erhalten.
Das war auch günstig für das Wachstum der Gruppe.
Auch wenn viele Kinder geboren wurden, so waren es viele Kinder, die vor dem Alter von vier Jahren starben.
Und daher wurden es nie zu viele.
