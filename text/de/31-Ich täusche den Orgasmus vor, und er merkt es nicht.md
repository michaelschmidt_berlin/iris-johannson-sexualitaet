## Ich täusche den Orgasmus vor, und er merkt es nicht

<div class="handwritten">
Nächste Frage:
Ich mag Sex nicht.
Mir fällt es leicht, erregt zu werden, aber dann wenn wir miteinander schlafen, dann möchte ich nur, dass es ein Ende haben soll.
Und da tue ich so, wie wenn ich einen Orgasmus habe, weil dann weiß ich dass er selber bald fertig sein wird und bekommt Samenerguss.
Und dann kann ich mich umdrehen und einschlafen.
Manchmal finde ich, dass es so sinnlos ist, dass es so sein muss.
Aber ich wage es nicht, etwas darüber zu äußern, denn ich mag ihn, und es ist schön, gemeinsam zu sein.
Wir fühlen uns wohl dabei, Dinge gemeinsam zu unternehmen.
Ich bekomme Angst, dass wenn ich nicht Sex mit ihm haben möchte, dann wird er mich verlassen.
Denn ich weiß, dass Sex haben für ihn wichtig ist.

Es wundert mich auch, dass er das nicht merkt, dass es nur getäuscht ist.
Er meint wirklich, dass ich einen Orgasmus habe.
Das verachte ich auch, dass er das nicht aufdeckt.
</div>

Es ist sehr häufig so, dass wir Frauen es so tun.
Wir spielen ein gefühlsmäßige Spiel, was zu Folge hat, dass wir eine Bumerangwirkung bekommen.
Das, was wir da tun uns enttäuscht macht.
Und da werden wir befallen von Schuldgefühlen und von schlechtem Gewissen.
Um das loszuwerden bekommen wir Verachtung gegenüber dem anderen, der uns ernst nimmt und uns glaubt.
Die Verachtung ist uns selber gegenüber, aber wir kehren das um zum anderen hin.
Und oft damit wir auffliegen, genau wie du sagst, wir möchten den Druck in uns selber herabmindern.
Wir möchten die Luft rauslassen in und selber, aber wir verstehen das nicht.

Männer können nicht das Gleiche tun.
Ihr Glied wird ganz einfach nicht steif und deshalb geht es nicht.
Sie müssen dazu stehen, dass sie nicht angezogen sind von dieser Frau.
Sie können das nicht auf die Weise vortäuschen.
Stattdessen werden sie oft wütend und gewaltsam.
Und wenn sie spüren, dass sie dadurch Macht bekommen, dadurch dass die Frau Angst bekommt, dann wird ihr Glied steif und sie bekommen ein Kick davon.
Und dann können sie den Beischlaf durchführen.
Und da nennt man es Vergewaltigung.

Der Grund weshalb unser Trieb so eingebettet in Gefühlspielen ist, ist, dass er ein Trieb ist.
Und dass er eigentlich genauso natürlich ist wie wenn das Herz schlägt und dass wir atmen.
Aber er ist verwickelt durch die Religion und dadurch dass er zur Folge hat, dass wir zu viele Kinder gebären, nach Bewertungen, die wir Menschen haben.
In der Natur gibt es nicht sowas, dass es zu viele werden.
Das heißt nur, dass die Schwachen sterben.
Und dann gibt es einen stärkeren Menschenstamm.
Aber wir haben eine Sichtweise auf den Menschen, alles Leben zu bewahren.
Und wir sind nicht versöhnt mit dem Tod auf die Weise wie die Ursprungskulturen es waren.
Deshalb kann es wichtig sein zu verstehen, dass es wir Menschen sind, die es verwickelt machen, uns selber.
Und dass dies am meisten sichtbar wird über die Probleme der Sexualität.

Möchtest du umdenken oder möchtest du nur eine Freundschaft mit deinem Partner haben?
Dies brauchst du zu durchdenken, um herauszufinden, wovon es dir gut ergeht, und gemäß dem Leben, ohne Schuldgefühle oder schlechtes Gewissen zu haben, denn das hat keinen Wert.
Der Wert liegt in der Beziehung, wie auch immer sie ist.
