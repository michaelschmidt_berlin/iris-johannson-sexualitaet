## Eifersucht, die Schwarz-Sinns-Seuche

Dann gibt es eine Schwarz-Sinns-Seuche. 
Und das ist etwas ganz anderes.
Das ist, wenn wir in einer Sackgasse innerhalb unseres Selbsts landen, und dort unsozial hoch operativ werden, und da kann es sein, dass wir tatsächlichen Schaden anrichten beim anderen, weil der Schutz- und Verteidigungsmechanismus im Autopiloten anspringt.
Und da sehen wir den anderen oder die andere nicht als einen Menschen, sondern nur als eine Bedrohung, als einen Gegenstand an, den wir aus dem Weg bekommen müssen.

Bei der normalen Eifersucht ist es einfach, den Fehler zu machen, dass wenn der andere mir so wohlgesonnen ist, dass er aufhört, dass die Eifersucht bei dir auslöst ausgelöst wird, und da ist es auf der Oberfläche weiterhin ruhig, aber es ist nicht in der Tiefe ruhig.
Da nimmt das Misstrauen zu und es kann sein, dass es dir vorschwebt, dass dein Partner dich hintergeht.
Da wird die Beziehung auf jeden Fall zerstört werden.

Das einzige, was trägt ist, dass man selber dieses magische Denken zerbricht, und dem Schrecken begegnet, damit die Eifersucht ausebben kann.
Oft gibt es eine verborgene Attraktion darin, eifersüchtig zu sein.
Da ist man hier und jetzt.
Man spürt die Lebenskraft bei der Wut.
Da fühlt man sich lebendiger als je.
Obwohl es auch so weh tut und so unheimlich unangenehm ist.
Daher gibt es auch einen Widerstand dagegen, diesen Strick loszuwerden.
Es ist schwer zu verstehen.

Wir fühlen uns oft fremd uns selber gegenüber, wenn es um die Eifersucht geht.
Es fühlt sich so an als wären wir ein anderer als wir selber.

<div class="handwritten">
 Ist das dein Ernst, dass ich es selber bin, der die beibehält?
Aus einem Grund, von dem ich selber nicht weiß.
</div>

Leider sind wir so unadequat, manchmal.
Obwohl es unlogisch ist und unmöglich selber zu begreifen.
Es ist wie wenn es eine destruktive Kraft wäre, die uns von außen vorgibt, was wir tun sollen.
Aber das ist es nicht.
Es hat mit unserem eigenen Lustzentrum zu tun, aus dem heraus die Anziehung entsteht.
Und was dafür oder dagegen wird, was sich da formt.
Manchmal ist es so, dass Eifersucht ausgelöst wird dadurch, dass es dem anderen gut geht, dass er oder sie zufrieden ist, froh und gerade offen und verletzlich ist.
Es wird so schmerzhaft, weil es nicht möglich gewesen ist, in der eigenen Kindheit es so gehabt zu haben.
Und oft ist es so, dass das, was einem am meisten gefällt bei dem Partner, das will man nicht, dass er oder sie haben soll nach einer Weile, weil andere auch diese Eigenschaft mögen.
Das ist ein schwieriges Dilemma.

Eifersucht hat so viele Gesichter, dass ist ganz unmöglich ist, an die einzelne Frage anders als ganz individuell heranzugehen.
Und ich hoffe, dass alle mit ihren eigenen Erfahrungen beitragen können.
Und mit ihren Geschichten beitragen können, um dieses Thema zu bereichern.

