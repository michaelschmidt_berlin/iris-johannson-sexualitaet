## Wie wähle ich meinen Lebenspartner?

Hast du noch weitere eigene Fragen oder wollen wir an dieser Stelle stehen bleiben?

<div class="handwritten">
 Ja, ich habe mehrere weitere Fragen von anderen bekommen.
Aber es sind Fragen, wozu ich mir auch Überlegungen gemacht habe.
Und ich habe es vor, sie direkt zu stellen.
Und hoffe, du gibst irgendeine, verschlauernde Antwort darauf.

Zum Beispiel, wenn ich meinen Partner wähle, was spielt dabei eine Rolle?
Ich meine, dass ich beobachtet habe, dass ich als Frau oft Ähnlichkeiten habe mit der Mutter des Mannes.
Und dass er, für den ich mich entscheide, ähnelt meinem Vater.

Das kann ja gut sein, aber es wird ja leicht so dass man dann irritiert wird wegen wegen der Person, gerade weil es einen an seinen Vater oder seine Mutter erinnert.
Ist dies wirklich ein geeigneter Grund für eine Paarbeziehung?
</div>

Es ist ja oft so, dass wenn wir einen Partner wählen gerade aus der Lust, so wird diese Lust oft hervorgerufen durch das, was bekannt und gewohnt und geborgen ist.
Und das, was uns am geborgensten ist, ist die Mitglieder der Kernfamilie.
Früher war es die Verwandtenfamilie, oder die Herde.
Deshalb sind wir oft geprägt durch den Geist unserer Mutter oder unseres Vaters.
Und oft ist es dies, was die Anziehung hervorruft.

Leider ist es so, dass wir genauso angezogen werden durch das Negative wie vom Positiven.
Weil im Grunde werden wir davon angezogen, was uns bekannt ist, was in dem anderen ist.
Dass wir meinen, dass der andere uns versteht, und das, endlich, fühlen wir uns gesehen und verstanden.
Leider ist es so, dass wenn der andere oder die andere den gleichen Mangel haben hat wie ich selber, so fühlt es sich so an in der Verliebtheit, aber wenn die Verliebtheit zu Ende gegangen ist, dann wird es nur leer.
Das Risiko besteht dann, dass man enttäuscht und kritisch wird gegenüber dem, was am Anfang die Lust hervorgerufen hat.
Und dann fangen die Probleme und die Gefühlsspiele an.
Und sie verbinden uns genauso sehr wie die Bereicherung und das führt zu einem einem Drama, was lange dauert.

<div class="handwritten">
Aber, ihhh, wie soll man dann machen?
</div>

Ja.
Zum Beispiel man kann an all die Leute denken, die man kennt.
Und sich und sich überlegen, ob einer oder eine davon in Frage kommt als Vater oder als Mutter der eigenen Kinder.
Oder es gibt jemanden, mit dem man immer gerne zusammen gewesen war.
Wer eine spannende Person ist.
Oder man findet jemanden, mit dem man einen Interessegebiet teilt, so dass man immer ein Thema mit jemandem teilen kann.
Vielleicht jemanden mit dem man Segeln kann, oder jemanden, mit dem man gemeinsam den Garten bestellen kann, mit dem man tanzen kann oder sowas.
Es gibt tausende Sachen, die das eigene Herz erwärmt.
Und der Mensch, durch den das Herz sich erwärmt immer und immer wieder, tja, vielleicht ist dieser der geeignetste nach Occam's Prinzip.

<div class="handwritten">
Was ist Occam's Prinzip.
</div>

Das Prinzip ist, dass aus mehreren wählbaren Alternativen wähle ich eines davon und bin der Meinung, dass es die beste Wahl ist, und dadurch sind die anderen nicht mehr wählbar.
Und ch meine, dass die Wahl die ich getroffen habe, die absolut beste ist, ohne Vergleich.
Dann stehe ich dazu ohne Bedingungen, bedingungslos, unbedingt.
Und wiederhole nie das Vergleichen mit denjenigen, die ich abgewählt habe.
Dann habe ich die Chance in einer glücklichen, eindeutigen Beziehung, ohne gekreuzte Finger und unnötigen Zweifel zu leben.
Und das Leben mit einem Freund teilen.
Freundschaft ist ja etwas, was von selber entsteht, während man Zeit miteinander verbringt.
Und dies als Grundlage eine Beziehung zu haben, ob das als Partner Beziehungspartner wird oder als eine separate Freundschaft, ist kein schlechtes Kriterium.
Die Freundschaft erträgt den anderen ganz anders als wenn die Sexualität dasjenige ist, was die Beziehung bestimmt.

<div class="handwritten">
Vielen Dank
</div>

