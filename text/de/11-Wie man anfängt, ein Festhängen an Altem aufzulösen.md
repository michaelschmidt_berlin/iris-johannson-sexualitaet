## Wie man anfängt, ein Festhängen an Altem aufzulösen

Wir üben auch nicht uns in der Kunst, den Trieb zu genießen als die Lebenskraft, die er darstellt.
Und ihn einfach die warme Quelle der Freude sein lassen, die es ist, diese Empfindungen zu haben.
Diese in einem da sein zu lassen, dass diese uns tief innerlich berühren, sinnlich, und sie unser Leben bereichern zu lassen.
Das unten stehende wrid eine Beschreibung der Sexualität öffentlich.
Das ist die Weltgesundheitsorganisation-Definition der Sexualität.

Die Sexualität wird erlebt und wird ausgedrückt durch Gedanken, Fantasien, Wünsche, Glaubensvorstellungen, Haltungen, Werte, Verhaltensweisen, Rollen und Beziehungen.
Auch wenn Sexualität alle diese Dimensionen umfassen kann, werden alle nicht erlebt oder ausgedrückt.
Die Sexualität hängt vom Zusammenwirken zwischen biologischen, psychologischen, sozialen, ökonomischen, politischen, kulturellen, ethischen, juristischen, historischen, religiösen und geistigen Faktoren ab.

Kein einziges Wort darüber, dass es der einzige Trieb des Menschen ist.
Und dass er ein tiefer tierischer, ein Dazukommen oder ein Aufschlag der Reproduktion, der Fortpflanzung ist.
Es ist vielleicht nicht die Antwort auf deine Frage, aber es ist ein Umstand, den man gerne im Bewusstsein haben kann, und der einen Sinn hat als ein Reichtum in unserem eigenen Denken.
Und ist etwas, worüber wir reden brauchen, um eine gemeinsame Sprache zu bekommen, so dass es leicht wird, mit anderen Menschen darüber zu sprechen bei verschiedenen Gelegenheiten.
Und zu wagen darüber Fragen zu haben, zu Verschiedenem.
In etwa, wenn wir mit unseren Kindern reden, oder über Gefühle sprechen.

Vor allem in Paarbeziehungen brauchen die Leute dies zu verstehen.
Normalerweise fange ich mit einer Frage an:
Ist es so, dass du oft ein Unbehagen oder einen Widerstand spürst, wenn es dazu kommt, dass dein Partner sexuelle Gefühle oder Bedürfnisse hat.
Spürst du dann irgendeine Form von Bedrohung oder von Angst?
Was dich dazu bringt, dich selber zu überfahren und du passt dich dem Mann an, weil sonst hört der vielleicht auf, dich zu lieben.
Oder, weist du die Situation ab, oder manipulierst du, so dass die Situation nicht entsteht.
Dann mache ich weiter damit.

Du brauchst mir keine Antwort auf diese Frage zu geben.
Du brauchst nur nachzudenken, wenn es so ist, dass du nicht einfach dem Menschen entgegenkommen kannst, 
das Gefühl bejahen kannst, und das Bedürfnis bejahen kannst, und dich selber erregt werden zu lassen.
Wenn dass sich so verhält, dann glaube ich, dass du feststeckst in einer alten Falle, in einem Gedankenfehler.
Und wenn du je irgendwann Hilfe bekommen möchtest, einen Prozess damit zu machen, so gibt es mich, und ich bin gerne dafür da.

Dann kann lange Zeit vergehen bis die Person innerlich sich soweit fühlt, mit mir darüber zu reden.
Aber oft für die dazu, dass die Menschen anfangen, mit ihren Freunden über dieses Thema zu sprechen.
Und das ist etwas Gutes, denn das ist der Anfang vom Entdecken, dass es nicht nur man selber ist, aber dass viele weitere Menschen in dem feststecken.
Und schon das hilft zum Teil, sich dessen bewusst zu werden.

Das gilt für vieles, was sich auf die Doppelheit des Triebs bezieht.
Einerseits dieser einfache, unkomplizierte, physische Grund, und anderseits diese verhärtete Beziehungsgeschichte, zu der es wird, wenn die Sexualität ein Teil des Ganzen ist.
Sex ist ein heißes Thema, das in den Filmen gezeigt wird.
Und über Sex wird in den Büchern geschrieben.
Es hat den Schein, so offen und zulassend zu sein.
Und gleichzeitig sind es so viele öffentliche Geheimnisse, die verdeckt werden, und die verboten sind, und wo es verboten ist, sie nur anzuschneiden in dem Intimsten der Beziehungen.

Es ist auch ein großes Unwissen über diesen biologischen Faktor, der so in einer ganz anderen Richtung geht, verglichen mit der Erklärung der Weltgesundheitsorganisation, die diesen Aspekt gar nicht mit dabei hat.
