## Die Kunst besteht darin, nichts zu tun

Du fragst was du stattdessen tun sollst.
Und diese Frage ist sehr gut, denn die Kunst besteht darin, nichts zu tun.
Nur beim Spielen sein, es sich tun lassen, was sich tun lässt.
Manchmal gut, manchmal weniger gut.
Es ist nur ein Spiel, das wir spielen, und was uns freut.
Oder das Spielen ist einfach ein Genuss oder bizarr und so weiter.
Genauso wie ursprüngliches Spielen ist.
Es entsteht.
Es geht vor sich bis es nicht mehr möglich ist.
Im Falle von Sex so kann dies sein, wenn der Samenerguss die Frau oder den Mann dazu bringt, dass die Lust dann erlöscht, wenn die Erregtheit weggeht.
Was jeder weiß, ist, dass die Lust immer wiederkehrt, immer und immer wieder.
Aber gerade jetzt ist sie nicht da.
Und das tun wir etwas anders währenddessen.

Wenn wir als Menschen darauf vertrauen können, dass es ein Trieb ist, und dass er von selber kommt von ihnen, wenn wir in uns selber sind, in dem Sein sind.
Und das gibt sich von alleine.
Niemand von den beiden muss denken oder bewerten.
Sie brauchen nur sein und die Nähe des anderen genießen.
Gerade dies, in sich selber sein ohne Vorbehalte, und dann in diesem Sein sich miteinander bewegen.
Wie ein Tanz, wie ein Spielen, wie ein Zusammensein und einfach genießen.
Dann wird es autonom.
Dann wird es wie es in der Tat ist ... ein Begattungs-Spielen ... könnte man das vielleicht übersetzen.

Ich habe ein wenig danach gesucht, eine sachliche Beschreibung was die sexualität ist zu finden, und da habe ich eine gefunden.
Sie beinhaltet so viele Wörter, aber unter den Wörtern gibt es keinerlei Andeutung, dass es ein Trieb ist.
Ich werde diese Beschreibung hier an anführen, damit du verstehst, wie verwickelt es ist in dieser Zivilisation.
Die Tatsache, dass der Mensch bestimmt wird durch diesen Trieb.
Wenn eine globale Einrichtung einmal dabei hat, was der entscheidendste Faktor ist, wenn es um die Sexualität geht.
Dass es ein Trieb ist und dass dies heißt, dass er nicht über das Bewusstsein geht, um sich einzuschalten, sondern dass er direkt aus dem Reptilienhirn oder aus dem Kleinhirn geht zum Impulshandlungssystem.
Dass dies der Grund dafür ist, dass wir so viel Angst bekommen vor dem Trieb, und dass wir uns darin so verwickeln, so schlimm darin verwickeln, anstelle davon nur einige einfache Tabus zu haben, von denen alle wissen und denen alle folgen wie Naturgesetzen.
"So tut man einfach nicht"
Ohne Bedingungen, ohne überhaupt in Frage gestellt zu sein und ohne Anpassung.
