## Es gibt nur einen Gott, und er ist ein Mann

Diese Standespersonen waren Imperialisten und ihr Machtgebiet war große Teile des Mittleren Ostens und des Nordafrikas.
Und unter ihnen Israel, wo das Judentum war.
Der Unterschied lag darin, dass das Judentum ein Ein-Gott-System hatte und dass dieser Gott ein Er war.
Dieser Gott wurde vertreten durch Männer.
Und es waren die Männer, die Menschen waren.
Die Frauen waren nur Frauen und etwas anderes als Männer.
Und sie hatten keine Bedeutung abgesehen davon, dass sie die Besitztümer der Männer waren und dass sie die Kinder der Männer gebären sollten.
Und dass der Mann wissen sollte, dass er der Vater des Kindes war, und aufgrund dessen entstand die Monogamie, in der Art wie wir sie kennen und wie sie sich gestaltet hat.
Das heißt, dass die anonyme Frau ihrem Herrn treu bleiben soll und keine anderen Männer haben soll.
Und wenn die Männer dann sie zu Dingen zwingen, oder gewaltsam ihr gegenüber sind, dann ist es ihr Fehler.

Und an diesem Punkt entstand eine Aufteilung des Menschlichen in einer ungesunden Weise.
Und die Sexualität zu einer geladenen Frage.
Das Misstrauen gegenüber anderen Männern veranlassten die Männer ihren Besitztum, ihre Frau, schützen zu müssen.
Und davon kommen alle die Beschränkungen, die wir heute noch sehen, in der Form, dass Frauen nicht ihre Haare zeigen dürfen und Kleider tragen müssen, die bis zu ihren Füßen reichen.
So wie es der Fall ist in einigen Religionenvarianten, die von der jüdischen Lehre ausgehen.

Da Männer sich nicht damit zufrieden geben konnten, ihre untergebenen, passiven Frauen zu haben, entstand die Prostitution.
Und sie bekam einen sündenhaften Hauch.
Nicht die Männer, aber die Frauen, die auf diese Weise ausgenutzt wurden, sie waren sündenhaft.
Die ganze Ein-Gott-Religion erschuf die menschlichen Gesetze unter den Bedingungen der Männer.
Die Männer entschieden was Sünde war in den Augen Gottes und was erlaubt war und fromm war.
Sex wurde zu einem Produkt, dass man verkaufen und kaufen konnte, und er wurde zu etwas Exaltiertem erhoben, was alle andere Vergnügungen übertreffen sollte.
Frauen wurde zu den Waren, die gekauft wurden und verkauft wurden.
Nicht der ganzen Frau galt das, aber es galt ihr Geschlecht.

Es wurden geheime Sekten gegründet, die eine eine Menge merkwürdige Bündnisse schlossen.
Sogar mit dem Teufel.
Der Hexentanz unter anderem.
Die Sexualität wurde mystifiziert.
Und sie wurde erhöht zu etwas anderem als das Sinnliche.
Und die Sexualität wurde zu einer Art von geistiger, heiliger Kuh aus der man das Beste herausmelken sollte.
Die Frau sollte den Mann befriedigen.
Die Frau sollte den Mann zufriedenstellend.
Und wenn sie das nicht tat, hatte er das Recht, sie zu verprügeln, sie zu verkaufen und sie zu töten.

Dies sind Überbleibsel, von dem, was noch da ist in Gewalttätigkeiten in nahen Beziehungen, wie man es heute heutzutage nennt.
Dieser Trieb wurde auch erhöht zu etwas Göttlichem, wie wenn die Männer nicht ohne auskommen konnten.
Dass dies schädlich war, wenn sie ohne auskommen würden.
Und dies legitimierte ihr Recht, Gewalt auszuüben, gewaltsam mit Frauen als Gegenständen umzugehen.
Später auch gewaltsam mit Kindern und anderen Menschen umzugehen, die in Abhängigkeit waren.
Beachte der Skandal, der nach und nach ans Licht gekommen ist, mit den katholischen Priestern, die jahrzehntelang die Knaben der Kirche sexuell ausgenutzt haben, weil es nicht in der Bibel steht, dass Gott das verboten hat.
Die ganze #metoo-Bewegung handelt von gerade diesem in einer modernen Version.
