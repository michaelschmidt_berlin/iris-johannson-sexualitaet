## Wer ist verantwortlich für Sexprobleme?

<div class="handwritten">
Nächste Frage:
Wessen Verantwortung ist euer Sexproblem?
</div>

Verantwortung ist kein Produkt, das man übernehmen kann oder wo man dem anderen Menschen die Schuld zuschieben kann, dass er nicht Verantwortung auf sich nimmt.
Verantwortung ist ein Zustand innerhalb und selber.
Der Zustand wächst und vertieft sich anhand unserer Erfahrung und unserem Wissen und unserer Fähigkeit.
Und daher haben beide diese Möglichkeit, frei Verantwortung zu tragen, das zu tun, was sie tun können.
Dann kommt ein Gefühl der Freiheit.
Und der Körper wird licht und zufrieden.
Danach streben wir das ganze Leben lang.
Es ist der gleiche Zustand, von der wir eine Erinnerung haben seit der Zeit in der Gebärmutter.
Und vor allem aus den ersten 14 Tagen, wo wir nicht angebunden sind am Träger, sondern frei schweben im Eileiter.
Wenn es zu dem Trieb Sexualität kommt, da haben wir selber zu regulieren, so dass es unserem Partner nicht Angst macht, wenn wir es sein lassen können.
Gerade dies, Angst zu bekommen vor einem Menschen in einer sexuellen Situation, macht, dass die Lust weggeht und ist oft der erste Schritt davon, dass die Beziehung ein Ende nimmt.
Und daher: Lebe verantwortlich, indem du zufrieden selber bist.
Und sei fürsorglich deinem Partner, liebevoll, und sanft, denn wie du weißt, ist der Liebesakt eine Situation großer Offenheit.
Dann wird das Leben zusammen am besten.
