## Kann Selbstbefriedigung eine Sucht sein?

<div class="handwritten">
Ich habe eine Frage bekommen von einem Mann, die ich gerne dich fragen würde:
„Ich habe mich während meiner Teenagerzeit sehr viel selber befriedigt.
Das war mein meine Art, Leben in mir zu spüren.
Aber ich meine, dass ich auch abhängig davon wurde.
Wie kann ich heute mit der Geschichte leben?
Wie soll ich tun, wenn dieser Sog anfängt in mir?
Ich kann mich ja nicht zurückziehen von den Frauen ganz und gar.“
</div>

Es ist oft, so dass Kinder sich reiben, gegen Tischbeine, Sessel, Matratzen und so weiter.
Das ist nicht Selbstbefriedigung, das ist sensitive, sinnliche Erlebnisse der Berührung.
Und sie kommen heran an die Zufriedenheit in sich selber dadurch.
Oft wenn sie müde oder überreizt sind, dann kommen Sie ihn sich selber hinein und fühlen sich wohl.
Wie du, der fragt, sagst, während der Teenagerzeit, wenn du geschlechtsreif wirst, so fängst du an, den Genuss der Selbstbefriedigung zu entdecken.
Und es ist ein Weg Spannungen loszulassen, die Unsicherheit zu lindern und die unbestimmte Angst darüber,
diese ganze fremde Welt, die sich in der Pubertät auftut.
Nicht abzusehen von dem Gefühlsstrom, der durch den Trieb rauf kommt und der lockt, verzaubert und Angst macht.
Wenn man dann sich selbst befriedigt, ist es ein Weg zu sich selber zu kommen.
Und es ist ein schönes Gefühl der Entspannung danach und man schläft gut ein.
Oder am Morgen fühlt man sich frisch.

An sich ist das harmlos und unschädlich, aber wenn man darin stecken bleibt, als ein Zwang, dann wird es zum Problem für einen selbst und vielleicht für die Umgebung.
Was man da von sich selber verstehen kann ist, dass man eine latente Suchtneigung hat.
Und wenn man das versteht, ist es einem vielleicht möglich, gefährlichere Suchtarten als diese zu vermeiden.
Es ist wichtig, Hilfe zu holen, jemandem zu erzählen, zu dem man Vertrauen hat, und Hilfe bekommen, wie es möglich ist, mit diesem weichen Stelle in der Konstitution zu leben.

Es ist wichtig für dich zu wissen, dass es sehr normal ist.
95 Prozent aller Männer befriedigen sich selber mindestens einmal pro Woche.
Und bei den Frauen ist die Zahl 72 Prozent.
Und deshalb ist es im Grunde kein Problem und du brauchst das nicht als etwas Schuld-Schamhaftes ansehen,was du bekennen brauchst.
Aber wenn es zum Zwangsverhalten wird, wenn es zu einem Wiederholungszwang bei dir wird, dann ist es ein Problem.
Und dann brauchen wir das anzuschauen, was du versuchst zu entkommen und in dir selber zu lösen, indem du dich selbst befriedigst.
