## Ein erster Kuss und der Weg zum Erwachsenwerden

<div class="handwritten">
Dann weiß ich, dass als ich in der vierten Klasse war, so gab es einen Jungen der zwei Jahre älter war als ich.
Er zog mich hinein hinter eine Hecke, die auf einem Grundstück neben der Schule stand, und er umarmt mich fest, und küsste mich auf den Mund.
Dauert der vielleicht nur eine Minute und dann rannte er weg.
Ich blieb stehen und kam in einen euphorischen Zustand, in einen Seligkeitsrausch.
Es entstand einen Sog und ein Pirren im ganzen Körper.
Und ich wusste nicht, was es war, oder was ich damit tun sollte.

Nach einer Weile war das Schlimmste vorüber, aber ist kehrte wieder zurück jedes Mal, als ich mich erinnerte an diesen Kuss auf meinem Mund.
Ich habe geraten, dass ich verliebt war, und ich fing an, mich für Mädchenliteratur zu interessieren für Bücher und Zeitungen und Fernsehsendungen, die Liebe zum Thema hatten.
Davor hatte ich nicht an meine Mutter und an meinen Vater gedacht als einzelne Menschen, als einzigartig und geschlechtsmäßige Geschöpfe, die eine Beziehung hatten.
Es ging mir auf und ich fing an, darauf zu schauen, wie sie sich gegenüber einander verhielten.
Früher war das nur eine Einheit gewesen, die zusammenhielt und die sich um mich und meine Geschwister gekümmert hat, die darauf schaute, das alles weiter ging und sie mischten sich sofort ein, wenn man etwas Dummes tat.

Meine Mutter nutzte die Gelegenheit und erzählte vom Weiblichen, dass ich wohl bald meine Periode bekommen würden und wie ich damit umgehen sollte.
Dass, wenn dies geschah, würden die Jungs Interesse an mir bekommen, auf eine andere Weise als jetzt.
Und dass sie Sex haben werden wollten mit mir.
Und dass es mir nach Gesetz nicht erlaubt war.
Erst wenn ich über 15 Jahre alt war.
Sie erzählte, dass man schwanger werden konnte und was das hieß.
Und dass man warten sollte mit dem Miteinander-Schlafen bis man sicher wusste, dass es ein Junge war, die man sich wirklich selber hingeben wollte, wie sie es sagte.

Da hörte meine Kindheit auf.
Sie wurde Geschichte, sehr abrupt.
Und im Leben fing es, an um Attraktionen zu gehen,
Attraktionen, die in mir hervorgerufen wurden durch manche Jungs.
Die Teenager Zeit fing an, als ich etwa 13 Jahre alt war.
Und sie dauerte bis ich den Mann kennenlernte, der mein Mann geworden ist.

Damals war ich 18 Jahre alt und ich hatte gerade angefangen mit der Hochschule.
Wir wurden uns einig, dass wir unsere Ausbildungen abschließen wollten.
Und währenddessen Zeit miteinander an den Wochenenden und in den Ferien verbringen wollten.
Und manchmal über eine Tasse Kaffee uns sehen würden in irgendeiner Kneipe.
</div>

Okay, was hast Du erlebt an Problemen in deinem Teenager- und in deinem Leben als Erwachsene, wenn es um die Sexualität geht?

<div class="handwritten">
 Ich habe eine ganze Menge Fragezeichen in bezug auf Vielem.
Und dies habe ich vor dir vorzulegen und dich zu bitten, darauf zu antworten.
Wir haben eine Gruppe, wo wir über Beziehungen diskutieren und über Schwierigkeiten in unseren Beziehungen diskutieren.
Und bei diesem kommen viele Sexprobleme heraus.
Und ich habe vor, diese Dir vorzustellen.

Wie ich verstanden habe hast du eine ganze Menge interessante Sachen, worüber du erzählen kannst, wenn es um nahe Beziehungen geht.
Und es sind ja in diesen nahen Beziehungen, wo die Probleme auftreten oft.
Sowohl zwischen Eltern und Kindern und wenn die Kinder junge Erwachsene werden, so werden die Eltern oft neidisch und verletzt, weil sie nicht meinen, dass der Partner oder die Partnerin von den Kindern gut genug ist.
So war es bei mir.
Meine Eltern stellten meinen Freund in Frage, und auch seine Gewohnheiten auf eine unfaire Weise.
Und das hat mich sehr verletzt.
Eigentlich tat mein Vater dies nicht.
Aber er hat immer gemeinsame Sache mit meiner Mutter gemacht und das tat er jetzt auch.
Ich habe wirklich mit ihm deshalb geschimpft und da wurde er sehr introvertiert, und hat kaum mit mir oder mit meiner Mutter geredet.

Ja, und dann meinte sie, dass es viel zu früh ist, dass wir uns verlobt haben.
Ihrer Meinung nach waren wir nur Kinder, immer noch.
Das hat mich zutiefst verletzt und ich zog bald aus und ein in einem Studentenwohnheim.
Und dort wohnte ich während meiner ganzen ganzen Studienzeit.
Obwohl ich an der Uni war meiner Heimatstadt.
Es ist schade, dass die Eltern so doof sind, dass sie der Grund sind, dass ihre erwachsenen Kinder Abstand nehmen von ihnen.
</div>

