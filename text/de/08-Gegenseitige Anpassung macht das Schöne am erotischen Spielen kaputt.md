## Gegenseitige Anpassung macht das Schöne am erotischen Spielen kaputt

<div class="handwritten">
Es scheint so weit hergeholt, aber wahrscheinlich gibt es eine Verbindung zwischen dem Alten und dem Neuen.
Aber wir sind ja moderne Menschen.
Und, meinst du, dass wir immer noch in dem alten, schiefen Denken drin sind, und kannst du noch etwas darüber sagen, welche die Folgen davon sind heute.
</div>

Das was, ich meine, oft Probleme verursacht für Frauen ist, dass sie auf irgendeine Weise immer noch sich anpassen an den Gedanken, dass der Mann Samenerguss bekommen soll.
Sie halten her, sie opfern sich, oder sie bieten sich an.
Sie machen da Anstrengungen in der Richtung, wirklich.
Wie wenn das Natürliche nicht wäre, dass der Mann seinen Akt mit Samenerguss abschließt.
Wenn es um den sexuellen Akt geht, immer noch meint die Frau, dass es an ihr liegt, wenn der Mann impotent wird.
Und das führt die Männer dazu an zu glauben, dass die Frauen es in der Weise haben wollen, dass sie so sind und das kultiviert die Sichtweise eines Objekts bei der Frau, wenn Sie auf sich selber schaut.
Und den Mann behält seinen vererbten Blick auf die Frauen bei.

Es steckt sehr tief und die Prägung ist hart, auf Zellebene, und es ist schwer da rauszukommen.
Auch wenn wir die Aufklärung im Kopf haben.
Viele Frauen suchen mich auf und sagen, hilf mir da raus, dass ich mich meinem Mann unterordne und wie ein Kind werde, wenn er schlechter Laune ist.
Ich tue mir Gewalt an.
Ich tue alles, was ich kann, damit er da rauskommt.
Sogar schlafe ich mit ihm vor dem Mittagessen, vor dem Abendessen, auch wenn ich gar nicht geil bin.
Habe nur Hunger.
Und ich tue so, wie wenn ich es wäre, die es will.
Ich werde wütend auf mich selber.
Dann kommt Verachtung ihm gegenüber auf, weil er so dumm ist, dass er drauf reinfällt.
Auch wenn ich weiß wie es wäre, wenn ich autonom bin, so schaltet sich dies ein.
Iris, ich möchte da rauskommen, und ich bekomme einen Abscheu vor Männern und für Sex und für mich selber und auch meinem Mann gegenüber und ich möchte nur flüchten, mich trennen.
Ich möchte nicht, dass er dieses Elend mit mir hat.
Was tue ich wohl?
Was tue ich bloß?
Und wenn es mit diesem nicht genug wäre, wenn nicht etwas haben möchte, wovon ich weiß, dass mein Mann das eigentlich nicht will, dass, dass wir das haben, so kann ich verführerisch sein und es trotzdem kriegen.
Er schmilzt dahin, immer, wenn ich so bin.
Pfui, ich hasse mich selber deswegen.

<div class="handwritten">
 Oh, ich erkenne mich darin wieder.
Ich weiß, dass meine Mutter so mitgemacht hat, weil sie wusste, dass mein Vater immer wohlgesonnen war, und sie konnte ihren Willen haben.
Und da konnte sie ihn um ihren Finger wickeln,
Und da konnte sie es so haben wie sie es wollte.
Schon als Kleines dachte ich, so würde ich nie machen, wenn ich sie wäre.
Aber ich bin damit geprägt, ich verstehe, dass es so ist.
Wie hilfst du jemanden, der in dem feststeckt?
Was hilft einem Menschen weg von dieser Versuchung.
</div>

Wenn wir in einer Paarbeziehung leben, dann haben wir, genau wie du, meistens diese Erwartungen aufgeschnappt.
Sie dünken uns wie ein ungeschriebenes Gesetz.
Und wir meinen immer noch, dass wir es sind, die dem Mann zufriedenstellen sollen.
Das meint der Mann auch oft.
Und dadurch sind wir uns zutiefst darin einig.

Es gibt eine subtile Anpassung in diesem, an eine Vorstellung, was wir tun sollen, und auch das Umgekehrte, was wir nicht tun sollen.
Dafür oder dagegen, aber es ist genauso angepasst, aber umgekehrt.
Der Mann soll die Bedürfnisse der Frau zufrieden stellen.
Er soll für sie alles tun.
Er soll auf sie schauen, als wäre sie ein edler Stein, eine Madonna.
Er sollte sie anbeten und so weiter.
In beiden Fällen schwindet die Lust aufgrund dieser Anpassung, dafür oder dagegen.

Am Anfang einer Beziehung, wenn das Paar verliebt ist, dann haben beide oft einen Sog, ein Pirren, eine Lust.
Sie spüren Verlangen nacheinander und sie geben sich dem erotischen Spielen hin.
Und sie gehen ganz darin auf, und ein Schimmern in vielerlei verschiedener Weise entsteht.
Jeder von ihnen tut alles, um dem anderen recht zu sein.
Oft kommt da Leisten-Wollen auf, beim Mann auf eine Weise und bei der Frau auf eine andere Weise.
Dieser Gedankenfehler, dass der eine das Bedürfnis des anderen zufriedenstellen kann, befriedigen kann, schafft eine Leistungserwartung, und macht das Schöne am erotischen Spielen kaputt.
Vor allem wenn die Verliebtheit dahin geschwunden ist.
Es sind viele Menschen, die dann Hilfe suchen.
Oder sie kompensieren mit Fremdgehen oder Prostitution.
Vielleicht auch mit anderen Suchtgewohnheiten.
