## Mein Partner ist so unsexy inzwischen. Was kann ich tun?

<div class="handwritten">
Wie antwortest du auf alle einfachen Sexprobleme, die Menschen so haben und die sie mitbringen?
Bei denen sie nicht so eine umständlichen Durchgang haben wollen, was du so schön machst.
Deine Antworten sind ja oft so triftig, aber die Leute wollen ja etwas haben, sodass sie wissen, was sie mit ihren Problemen unternehmen sollen.
Und da bin ich neugierig was du sagst, wenn diese Fragen jetzt an dich getragen werden.
</div>

Du kannst mich gerne fragen und ich werde so einfach wie möglich antworten.
Manchmal ist ein kleiner Umweg notwendig, damit es möglich ist ein Verständnis zu bekommen bei etwas, was wichtig ist.
Aber manchmal sind es einfach einfache Antworten, die machen, dass wir im Moment Hilfe bekommen.
Etwas, womit wir probieren können.

<div class="handwritten">
Okay, dann antworte auf diese Fragen.
Ich werde sie so stellen wie wenn ich es bin, der sich stellt, okay?
Ich habe nicht mehr Lust zum Sex und vor allem nicht mit meinem Partner.
Mittlerweile ist er so unsexy.
</div>

Wenn du das ändern möchtest, so brauchst du etwas damit unternehmen, was macht, dass du es gut mit dir hast.
Dass du gut riechst, dass du schaust, dass er gut riecht.
Zum Beispiel könntet ihr gemeinsam baden.
Wenn es einfach das ist, dass du keine Lust bekommst so unternimm spannende Sachen, die ihr gemeinsam machen könnt.
Am liebsten etwas, was euch beiden gefällt.
Vielleicht ein Event besuchen und dann diese Anreize gemeinsam erfahren.
Oder ihr könnt mit eurem Auto einen Ausflug machen und miteinander reden.
Oder, ja, was auch immer unternehmen, was deine Lust aufweckt, und dann auf ihn überfließt.

Aber wenn da ein Konflikt ist, unterhalb deiner Unlust, so ist es wichtig, dass du diesen hervorlässt und ihn löst.
Der Konflikt ist nicht zwischen euch, sondern er ist eine Ladung in dir selber.
Und diese Ladung brauchst du zu bearbeiten, um da raus zu kommen, so dass die Unlust loslässt.

