## Mein Glied will nicht mehr aufrecht stehen. Was kann ich tun?

<div class="handwritten">
Nächste Frage.
Ich bin ein Mann in meinen besten Jahren, aber mein Geschlecht wird nicht steif.
Mein Glied will nicht mehr aufrecht stehen.
Was kann ich dagegen tun?
</div>

Wenn es nicht an mangelnder Lust liegt an Sex, so brauchst du zu einem physischen Arzt gehen und dich untersuchen lassen.
Es gibt eine ganze Reihe von Gründen, zum Beispiel verschiedene Stressfaktoren und Hormonausfälle unter anderen.
Das ist meine erste Empfehlung an dich.
Und wenn kein physischer Grund vorliegt, dann würde ich anfangen, deine Lebensumstände anzuschauen.
Wo befindest du dich in deinem eigenen Leben?
Hast du alle deine Ziele erreicht und es gibt bei dir keine Richtung?
Vielleicht sind deine Kinder ausgezogen oder du und deine Frau haben nicht mehr etwas Gemeinsames, mit dem ihr die Freude teilen könnt.
Alles ist nur Routine und Rituale, leere Hüllen und so weiter.

Erst feststellen, dass es ein Problem gibt, dann kann vielleicht ein Therapeut das Richtige sein, oder eine Gruppe, wo die Leute auch so ähnliche Probleme haben.
Oder am liebsten durchspreche das mit deinem Partner und bitte sie um Hilfe, sodass ihr gemeinsam Lösungen findet und mit diesen spielt.
