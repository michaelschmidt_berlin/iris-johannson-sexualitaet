## Ich bin gemein zu meinem Partner, aus Angst vor Nähe

<div class="handwritten">
Wenn man gemein ist den eigenen Partner gegenüber, weil man eigentlich Angst hat vor der Nähe und versucht gegenüber dem anderen auf Abstand zu gehen oder die Beziehung zu beenden, obwohl es das eigentlich nicht ist, was man will, und trotzdem merkt, dass man ständig gemein ist.
Was kann man da tun?
</div>

Hol dir Hilfe, sobald du kannst, Hilfe für dich selber, und geh zu jemanden, der dir helfen kann zu verstehen, dass dein Autopilot, dein Schutz- und Verteidigungssystem fehlprogrammiert ist.
Erzähle es deinem Partner oder deiner Partnerin auch, und sage wie es ist, dass du nichts mehr möchtest, als mit diesem oder mit dieser zu sein.
Und du möchtest nichts lieber als lieb und warm und nett zu sein und dass du nicht es steuern kannst, dass du gemein wie eine Hexe bist stattdessen.
Dass du wünschst, dass dein Partner oder deine Partnerin es aushalten soll mit dir, trotz diesem.
Bis du zurecht gekommen bist mit deinem Problem, und deinen Partner oder deine Partnerin es frei steht, wütend, traurig zu werden oder Angst zu bekommen, oder was auch immer zu werden und einfach dich auslachen.
Und dass du den Weg findest dazu, so zu werden wie du bist, ohne das Anspringen deines Ängstesystems.
Wenn ich Dich kennen würde, würde ich zu dir sagen, dass du teilnehmen solltest an einem einer Lebenspielgruppe.
Weil das ist eine Weise, es in der Tiefe zu verändern.
Und als Ergänzung dazu ein Zuhörerabonnement zu haben, was ich früher beschrieben habe.
Dieses Benehmen sitzt zu tief, dass es nicht möglich ist, mit Wörtern dorthin zu kommen, mit Tricks, mit Methoden oder Techniken.
Das setzt auf primärer Ebene in deinem intuitiven System.
Und alles Heilen und alles Kurieren macht der Körper selber, wenn er die richtigen Voraussetzungen bekommt.
Aber man kann es nicht erwarten, einfach geradewegs hinein in das Problem gehen und schaut was passiert und sich verändern lassen.
Na, jetzt habe ich alle deine Fragen beantwortet die du angehäuft hattest, auch von Seiten anderer.
Und dann bleibe ich hier stehen.
Reicht es so oder gibt es etwas anderes, an dass du denkst, und was du aus meiner Perspektive gespiegelt haben möchtest gerade?

<div class="handwritten">
Es ist soweit gut und ich verstehe, dass sich das Ganze von Anfang an lesen brauche, damit die kurzen Antworten veständlich werden.
Es ist nicht einfach, das eigene Triebleben zu verstehen.
</div>

Doch im Grunde ist es sehr einfach, aber es ist nicht leicht.
Und es wird mehr und mehr kompliziert.
In der verwickelten, sekundären Zivilisation, wo das Sekundäre leicht den Platz des Primären einnimmt.

Meine E-Mail-Adresse: iris@irisjohansson.com.

<div class="page-break-before">&nbsp;</div>
