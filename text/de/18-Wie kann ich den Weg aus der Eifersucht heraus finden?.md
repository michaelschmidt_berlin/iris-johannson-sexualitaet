## Wie kann ich den Weg aus der Eifersucht heraus finden?

<div class="handwritten">
Was tue ich denn und was kann jeder tun, der eifersüchtig wird.
Ich finde das so hinterhältig, denn manchmal kann sie so vollkommen jeden vernünftigen Gedanken ausschalten und jede vernünftige Einsicht ausschalten, die da ist bei mir.
Ich werde einfach so wütend und will diese Person umbringen, die meint, dass es meine Schuld ist.
Und die mich im Stich lässt.
Ich sehe ein, dass es nicht an dieser Person liegt, aber ich werde so verrückt, weil er darauf hereinfällt, weil er bezaubert wird durch den Scharm und auf das Einschmeicheln.
Ich werde eigentlich wütend auf sie, weil ich denke, dass sie das bewusst mir gegenüber macht.
Und dennoch weiß ich, dass sie das nicht tut.
Sie ist nur wie sie ist.
</div>

Das erste was du tun brauchst ist zu verstehen, dass du Angst bekommst.
Todesangst, wie wenn es um Leben oder Tod gehen würde, wenn dein Freund dich verlassen sollte.
Oft haben wir diesen Schrecken bekommen dadurch, dass wir bedroht worden sind durch die Bedrohung unserer Mutter oder unseres Vaters, das dann darfst du nicht mitkommen, als wir klein waren.
Oft vor dem Alter von 10 Jahren.

Dann ertragen wir nicht gefühlsmäßige Bedrohungen, verlassen zu werden.
Außerdem meinen wir, dass der andere uns umbringen wird, wenn wir einen Fehler machen, und den Elternteil nicht besänftigen können, wenn ein Elternteil aggressiv ist.
Es ist nicht möglich für ein Kind zu verstehen, dass der Elternteil dann Angst hat, sondern wir meinen, dass sie auf uns böse sind.
Auch wenn Eltern unter sich Spiele treiben, bekommen wir Angst, dass Sie uns verlassen werden, und da versuchen wir, recht zu sein lieb zu sein und uns ausmerzen, um gut genug zu sein oder zu taugen.
Das bleibt dann hängen.
Und das gebärdet sich oft in einer Paarbeziehung.

Die Spuren, die dies hinterlässt, sind so tief, dass wenn wir in einer Beziehung mit einem Partner sind, vor dem wir offen und verletzlich sind, und der das Licht in unserem Leben gerade da ist, dem wir vertrauen ... dann, dann befällt uns da die gleiche Finsternis als es tat als wir Kinder waren.
Und wir reagieren genauso primitiv wie damals.
Und wir versuchen dem auf Unmengen von Weisen zu entkommen.
Auf die Weisen, die wir gelernt haben als Kinder, wie wir uns entziehen konnten.
Zum Beispiel, indem wir nicht mehr fühlen.
Es ist nichts, was wir wählen, aber oft meinen wir, dass wir diese Wege wählen.

Die Art wie wir wählen kommt oft aus dem Autopiloten heraus, nicht aus dem Bewusstsein heraus und aus dem Intellekt.
Wenn du einmal verstehst, dass du Todesangst hast, kannst du denken:
Wie soll ich mich jetzt um mich selber kümmern, jetzt wenn ich Todesangst habe.
Todesangst, sie oder ihn zu verlieren.

Wenn du einfach diesen Gedanken denken kannst, kannst du ihnen wechseln, und dir gestehen wie es wirklich ist.
Und gerne dann auch für deinen Partner „ich fürchte meines Lebens, wenn du so bezaubert wirst von ihr. Weil dann glaube ich, dass ich dich verlieren werde an diese Person. Und da ist so viel starben dabei, dass ich so egoistisch denke. Und es fällt mir so schwer, dazu zu stehen, dass ich auf diese Weise reagiere.
Ich möchte nicht, dass dir dies etwas bedeuten soll, und ich möchte nicht, dass du dann was anders machen sollst
Ich möchte nur, dass du weißt, dass mich die Eifersucht befällt, und dass ich mich da so kindisch benehme“.

Dann kann es sein, dass du jemanden brauchst, bei dem du entladen kannst.
Jemand, der zuhören kann.
Und dass du deine Gefühle wiederkauen darfst, bist du etwas findest, was macht dass es leichter wird und dass du aufwachsen kannst und reifen kannst, so dass du nicht wieder in dieser Falle landest, immer und immer wieder.
Das geht meistens nicht sofort, aber wenn du dabei bleibst und dir selber deine Eifersucht vorhälst, so ist es oft so, dass es schneller und schneller vorbeizieht und am Ende dich nicht besonders hart trifft.

