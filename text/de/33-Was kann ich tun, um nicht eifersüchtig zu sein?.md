## Was kann ich tun, um nicht eifersüchtig zu sein?

<div class="handwritten">
Was kann ich tun, um nicht eifersüchtig zu sein?
Ich merke, dass es mir ganz schwarz wird innen.
Wenn jemand seine Wertschätzung meines Partners zeigt und sich benimmt, wie wenn es mich nicht gäbe.
Er oder sie sieht nur meinen Partner und hat Scham und ist entzückend.
</div>

Geh weg und fange an, mit einem anderen Menschen Zeit zu verbringen.
Interessiere dich für etwas ganz anders, was dein deine Sinne engagiert, und was du gerne hast.
Wenn du auf einer Party bist, so schaue dich um nach einem anderen Menschen, mit dem du Zeit verbringen kannst währenddessen.
Und knüpfe wieder an deinen Partner an, wenn er oder sie wieder alleine ist.
Wenn es Musik gibt, zum Zuhören und die du genießen kannst, so gebe dich der hin.
Oder finde jemanden, mit dem du tanzen kannst.

Wenn Kinder in der Nähe sind, so gebe dich dem hin mit ihnen zu spielen.
Ich weiß, dass du keine Lust hast zu irgendetwas von dem, was ich vorschlage, sondern einfach ihm die Augen aus dem Kopf rausreißen möchtest, dem Menschen, der deinen Partner so sehr schätzt.
Aber es ist das, von dem du dich wegzwingen sollst.
Und ich gebe lediglich einige Beispiele davon, wie du dich davon wegzwingen kannst.
Dann ist es wichtig, dass du deine Eifersucht nicht bei deinem Partner rauslässt, denn das ist der Anfang vom Ende eure Beziehung.
Das macht Angst, und ein Mensch der Angst bekommt, wird früher oder später in die Anpassung gehen.
Und das erträgt er nicht.

Eifersucht auf der Weise sind nur Hirngespinste.
Du meinst, das der andere etwas bekommt, was das ersetzt, was du gibst.
Das ist ein Besitztumdenken, das keinen Platz hat in einer Beziehung, nicht einmal wenn man ein Paar ist.
