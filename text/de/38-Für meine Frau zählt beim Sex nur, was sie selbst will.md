## Für meine Frau zählt beim Sex nur, was sie selbst will ...

<div class="handwritten">
Nächste Frage:
Meine Frau ist so egoistisch.
Für sie zählt nur sie selber, wenn es zum Sex kommt.
Ich möchte mich darum kümmern, wie es ihr geht, und ich möchte es ihr schön machen.
Aber sie kümmert sich nicht darum, gar nicht, wie es mir geht oder wie es mir recht ist.
Und daher entsteht für mich keine Beiderseitigkeit, nur Sex, und daran habe ich so wenig Interesse.
Ich möchte Beziehung, ich möchte es gemütlich haben, kuschelig, nicht einfach so davondüsen durch einen gewaltigen Sex.
Und das ist ihr Interesse.
Dann hat sie Sex gern.
Sonst können wir es sein lassen.
Wie löse ich das?
</div>

Erstens ist es wichtig, dass du zufrieden bist damit, dass du unzufrieden bist.
Denn das ist adäquat, unzufrieden zu sein.
Sie will es vermutlich so haben, wie ihr es jetzt habt.
Und du möchtest es verändern.
Wenn du unzufrieden bist damit, dass du unzufrieden bist, da entstehen nur Widersetzlichkeiten und Krieg.
Aber wenn du zufrieden bist damit, dass du unzufrieden bist, dann könnt ihr miteinander sprechen über das Phänomen Erotik auf eine andere Weise.

Was hinter ihrem Abstand zu dir in intimen Situationen ist, weiß weder du noch sie.
Ihr wisst nur, dass es etwas gibt, was sie weglenkt davon, in der Offenheit zu sein.
Und du kannst sie danach fragen, wenn du zufrieden bist und kein Investment hast in die Antwort.
Dann, vielleicht nach langer Zeit, kann sie etwas über sich selber sagen.
Warum sie dir nicht entgegenkommen möchte.
Vielleicht war das so bei ihren Eltern.
Oder sie hat gelesen, dass es so sein soll, oder von wem hat sie ihr Bild?
