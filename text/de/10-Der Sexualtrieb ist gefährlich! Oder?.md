## Der Sexualtrieb ist gefährlich! Oder?

Der Trieb kann außer Gefecht gesetzt werden.
Und da wird der sexuelle Impuls schädlich, zum Beispiel Inzest, Vergewaltigung, Gewalt in nahen Beziehungen, Totschlag, Eifersucht,gefühlsmäßige, existenzielle Spiele die mit Totenhaus {auf dem Friedhof, im}, Irrenkrankenhaus oder Gefängnis enden.
Wir erschrecken in unserem Innersten, da wir nicht wissen wie wir reagieren können, wenn wir in dem Nebel des Unbewusstseins landen und dort ein Einschalten von dunklen Triebsimpulsen bekommen.
Wenn dir geborgen sein können mit dieser Ungeborgenheit, dass wir den Trieb haben, aber dass wir lernen können, alle Handlungen, die der Trieb in Gang bringt, abzublasen.
Dass wir nicht dem Trieb ein Opfer sein müssen.
Dann ist er einw Resource und eine Lebenskraft.
Aber, da es ein Trieb ist, springt er in vielen unpassenden Situationen an, und es ist dies, welches der Grund ist, dass wir so viel Angst vor dem Trieb haben.
Weil wir Angst haben, dass er überhand nehmen wird, dass wir peinlich werden können, dass wir vielleicht etwas nicht richtig deuten und enthüllt werden, und dass der Trieb uns zu etwas hin treiben kann, worüber wir keine Macht haben, und demgegenüber wir nicht sagen können, dafür stehe ich.
Oder dass er uns dazu treiben kann, etwas zu tun, was nicht erlaubt ist.
Dass wir etwas tun könnten, was uns zum Schämen bringt.

Es ist sogar Scham, sich zuzugeben, dass wir Menschen sind, mit einem Trieb, der sich so oft einschaltet, wie er es tut.
Wir wissen auch nicht, was wir tun sollen mit den Reaktionen bei uns selber, wenn wir in Kontakt kommen mit dem Trieb von anderen.
Wir landen dann in dem Schutz-und Verteidigungssystem des Autopiloten.
Wir werden ganz eingefahren, wir werden da ganz starr im Denken, im Fühlen und im Handeln.
Nichts ist möglich, wir werden paralysiert, und kommen in ein Außer-uns-selber-System hinein, was zufolge hat, dass der andere „was auch immer“ mit unseren Körper tun kann.

Viele von denen, die Opfer gewesen sind, des Inzestes, der Vergewaltigung, die geschlagen werden und so weiter, sie zeugen davon.
Deshalb brauchen wir zu lernen die andere Seite des Autopiloten zu programmieren.
Dass wenn wir etwas Unangenehmem ausgesetzt werden, was aus der Sexualität des anderen herkommt, dann schreit man, man reißt, man beißt, man tritt, man macht sich los und rennt davon.
Dann gelangt dieser Antagonist in eine Paralysierung und dann ist die Gefahr vorüber.

Früher wurde dies kleinen Kindern beigebracht.
Aber diese Initiation ist nicht mehr da.
Und eigentlich müsste sie wieder aufleben, wenn man an das Ausmaß der m#etoo-Bewegung denkt.
In so einem Fall, dass man bekannt und gewohnt wird an dem eigenen Trieb, dass er kommen und gehen kann, wie er beliebt, ohne da reinzufallen in das Machen dessen, tun zu müssen.
Und das wäre erleichternd, nicht auf den Trieb hin etwas unternehmen brauchen.
Es ist dies, worüber viele nicht aufgeklärt sind.
Und deshalb werden bekommen sie vor diesem Trieb so viel Angst.
Angst, dass sie sich selber in manchen Situationen handhaben werden können, oder mit sich selber umgehen können.
Leider üben wir selten, mit den natürlichen Schwierigkeiten im Leben umzugehen.
Wir möchten sie nur aus dem Weg haben, dass sie nicht mehr da sein sollen.
Das kann ja bei vielem der Fall sein, aber das gilt nicht für Triebe und für Sucht.

