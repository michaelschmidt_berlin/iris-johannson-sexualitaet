## Wie wählen wir unsere Sexualpartner?

<div class="handwritten">
Ja, genau. Das ist auch was zum Denken.
Aber in mir ist es so unbegreiflich, einen Partner mir vorzustellen, bei dem ich nicht erotische Anziehung schon von Anfang an spüre. 
Das scheint unfair, in dem nicht zu sein, mit jemandem zu sein, der den nicht dieses Pirren und diesen Zog in mir aufweckt.
Jemanden zu wählen aus einem anderen Grund, das fühlt sich falsch und Lügenhaft.
Wie wenn ich dann jemanden anschmiere, eine Beziehung mit mir anzufangen.
Ich bin damit indoktriniert, wie kann es möglich sein, dass ich was anderes weiß?
Ich weiß, dass viele meiner Freunde in gleiche Weise denken.
Und deshalb möchte ich das gerne irgendwie geklärt bekommen.
</div>

Der Grund, dass wir heute so räsonieren, so denken, ist dass wir nicht verstehen, dass die Sexualität ein Trieb ist.
Und dass er immer aufgeweckt werden kann, wenn wir einander nahe kommen, wenn wir die Worte schätzen des anderen und seine oder ihre Ansichten.
Wenn wir von unseren Sinnen Gebrauch machen, in der inneren Zufriedenheit, die wir selber haben, zu sein.
Dann gibt es nichts Besseres als jemanden zu haben, mit dem man die ist teilen kann.
Und dann, ja dann werden wir sensitiv.
Und da gießt sich die Sexualität hinein und wir fühlen uns erotisch angezogen.

Es kann die Art sein, wie ein anderer Mensch lächelt, wie er riecht, die wonnigliche Stimmlage oder die Melodiösheit der Stimme.
Es kann die tänzerische Bewegung, wenn jemand geht, sein, und so weiter.
Es setzt voraus, dass wir uns nicht aneinander anpassen, dass wir die Bedürfnisse des anderen nicht befriedigen sollen, sondern dass wir stattdessen das Bedürfnis des anderen so zum eigenen machen, dass wir eine Gabe bekommen, mit der wir zufrieden sind.
Dann können wir so tun wie der andere es möchte, ohne uns anzupassen.
Und dann entsteht nicht der Widerstand in uns selber, gegenüber der Sexualität in der Beziehung.
Wir meinen oft, vorurteilsweise, das abgemachte Ehen, nicht glücklich sein konnten, denn sie fingen nicht mit der Attraktion an.
Aber zum einen war es so, dass diese Ehen sehr wohl darauf aufbauten, dass die Familien einander kannten, und es war sehr wohl passend, jemanden zu heiraten, der den gleichen Hintergrund hatte.
Und da ging es gut, miteinander klarzukommen.
Wenn es um die Jungen ging, da wussten die Erwachsenen oft aus eigener Erfahrung, dass wenn sie nur einander sehen würden und Zeit miteinander verbrachten, wenn andere dabei waren, so würde die Anziehung und das Verlangen sich einstellen, da würden Sie nach Sex sich sehnen.
Und da würden sie heiraten wollen, da ihnen nicht erlaubt war, Sex miteinander zu haben, bevor sie Heirat verheiratet waren.
Und in der Tat, es entstanden genauso viele glückliche und wohl funktionierende Beziehungen durch dieses Verfahren wie in der Art, wie wir es jetzt machen.
Wenn wir eine Beziehung auf etwas so Flüchtiges aufbauen wie auf die sexuelle Attraktion,
ich befürworte nicht das eine oder das andere, ich mache nur deutlich wie der Trieb funktioniert.

Wir sind alle Menschen und wir haben diesen Trieb, aber es ist eine der wenigen Sachen im Leben, wo es uns unterscheidet als einzigartigen Menschen wie wir gemäß dies funktionieren.
Ich bin nicht so sicher, dass es aus der Konstitution kommt, sondern es kann genauso gut aus Umwelt oder umgebungsmäßigen Gründen sein.
Und aus traditionellen Strukturen, die die angelernt werden über die Generationen.
Ich werde einige von ihnen hier nennen, um auf die Ähnlichkeiten und Unähnlichkeiten zu schauen.
Und dennoch ist es der Trieb, der auf eine ausgeklügelte Weise hilft, Beziehungen dazu zu führen, Kinder in die Welt zu setzen.