## Über die Entdeckung des Geschlechts in der Kindheit

Okay, jetzt kommen wir dazu, wirklich über die Sex-Probleme zu reden, die Probleme, die die Menschen haben, und was ihnen so viel zerstört oder verstört in dem Zusammenleben bei ihnen.
Und ich würde gerne diese Sachen spiegeln auf die Weise, auf die ich es tue.
Ich hoffe, du stellst alle die Fragen, auf die du eine Antwort haben möchtest, denn jetzt weißt du etwas über meine Referenz, wenn ich sie beantworte.

<div class="handwritten">
 Ja, es berührt mich, all das, was du bis jetzt gesagt hast hast.
Das fühlt sich ein bisschen komisch an, denn im Grunde so weiß ich dies und dennoch weiß ich dies nicht.
Es ist so schwer, wenn die ganze Umgebung von einem etwas anderes sagt und tut und obwohl man es selber besser weiß, so tut man selber auch etwas anderes.
Ich verstehe, dass ich so fest durch diese Kultur geprägt worden bin und durch die Menschen, die um mir sind, durch die bin ich auch so geprägt.
Aber dennoch finde ich, dass ich mehr mich selber sein sollte und sein müsste.
Und dass ich nicht Opfer fallen müsste dieser existenziellen Angst.
Die so leicht durch die Sexualität eines anderen hervorgerufen wird.
Und die zu einem so sehr großen Problem in meinen nahen Beziehungen wird.
</div>

Okay.
Kannst du dich erinnern wie das war, als du selber klein warst.
Wann hast du angefangen, daran zu denken, das Kinder unterschiedlicher Geschlechter waren.
Dass sie unterschiedlich waren und dass man selber etwas anderes war als dem gegenüber.

<div class="handwritten">
 Es war als ich anfing mit der Schule.
Früher im Kindergarten, so hat sich niemand darum gekümmert, ob es Jungs oder Mädels waren, die miteinander gespielt haben.
Es waren Jungs, die in der Puppenecke dabei waren.
Und es waren sehr viele Mädchen, die Fußball gespielt haben und die miteinander gerungen haben, die miteinander Fangspiele gemacht haben und Ballspiele gemacht haben.
Und andre Spiele gemacht haben, wo wir sehr viele waren.
Aber oft waren es die Jungs, die über die Regen entschieden.
Und ich weiß, dass ich das sehr gern hatte.

Dann als wir anfingen, zur Schule zu gehen, dann sollten die Mädchen eine Zeit in der Sauna bekommen, wo nur die Mädchen unter sich waren.
Und die Jungs sollten eine andere Zeit haben, wo sie nur unter sich waren.
Wir sollten getrennte Zimmer haben, wo wir uns umkleiden konnten vor dem Turnen.
Und wir wurden zusammengepfercht, so dass die Mädchen unter sich waren und die Jungs unter sich, bei verschiedenen Zusammenhängen.
Da weiß ich, dass ich anfing, die Jungs mit anderen Augen anzuschauen als ich es früher getan habe.
Dann, ganz plötzlich, wurde etwas fremd im Menschsein, Kind zu sein, es entstanden zwei verschiedene Gruppen, Jungs und Mädchen.
Ich glaube nicht, dass es mir dämmerte, dass es mit Sex zu tun hatte.
Aber irgendwie sollte man auf eine gewisse Weise sein, wenn nur Mädchen da waren, als wenn sowohl Mädchen als auch Jungen Jungs da waren.
</div>

Es ist das erste Zeichen der Entwicklung, dass man bewusst wird, dass es zwei verschiedene Arten von Menschen gibt.
Und dass dies in manchen Zusammenhängen eine Bedeutung hat, aber meistens hat es das nicht.
Wenn ihr Dinge tut als Menschen gemeinsam, dann hat es keine Bedeutung, nur kulturell wird es getrennt.
Nur kulturell werden Jungs und Mädchen voneinander getrennt.
Kameradschaft, Kinder zwischen Kindern, da wird kein Unterschied gemacht bei Jungs und Mädchen, sondern sie sind unbehindert miteinander.
Aber schon gibt es mädchenhafte Mädchen, die Jungsspiele nicht verstehen.
Und es gibt Jungs, die meinen, dass Mädchenhaftigkeit lächerlich ist, und unheimlich ist irgendwie.
Die Frage ist, woher sie diese Vorurteile aufgeschnappt haben.
