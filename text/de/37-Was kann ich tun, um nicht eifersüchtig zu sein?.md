## Was kann ich tun, um nicht eifersüchtig zu sein?

<div class="handwritten">
Nächste Frage:
Ich habe gespürt, wie etwas anspringt der erotischen Natur in Beziehung zu meinem kleinen Kind.
Ist das irgendwie krank?
</div>

Nein, gar nicht.
Dass wir solche Lustgefühle von Musik bekommen, von der Kunst, vom Spielen mit Kindern, von dem, was schön ist und von dem, was uns anspricht, ist ganz in Linie mit dem Trieb.
Er will sich hinein ergießen in alles, was möglich ist.
Weil er will die Reproduktion.
Und es sind wir selber, die darauf verzichten, nach ihm zu handeln.
Und auf die Weise ist er nicht schädlich.
An sich ist er lebensbejahend und gibt Kraft und Inspiration.
Lass dich also durch in bereichern.
Aber lade ihn nicht mit sexuellen Handlungen auf, sondern lass ihn von selber weggehen, nach einer Weile, und dann hast du Fokus auf anderes im Leben.
Dies ist nichts, wovor du Angst haben brauchst.
