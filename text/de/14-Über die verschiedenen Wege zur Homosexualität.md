## Über die verschiedenen Wege zur Homosexualität

Ich habe mir viel überlegt, warum manche Menschen homosexuell werden, und warum es so wichtig für sie ist, sich zu outen, wie sie sagen.
Kann es daran liegen, dass sie einen Konflikt haben mit ihren Vorbildern, mit ihren Eltern.

Es können mehrere Gründe sein.
Ein Grund ist biologisch und hat mit dem Genetischen und mit den Hormonen zu tun, dass es aus irgendeinem Grund so ist, dass man nicht angezogen wird, Lust bekommt zum anderen Geschlecht.
Aber Lust bekommt auf jemanden beim eigenen Geschlecht.
Dies kann man nicht überwinden, auch wenn wenn man lange Zeit meint, dass man das wohl einfach überfahren kann und dass man sich eine Frau oder einen Mann finden kann.
So ist es nicht.
Früher oder später entsteht ein existentieller Konflikt innen.
Dem folgt eine Depression.
Und da bleibt oft nicht die Beziehung, die man hat, bestehen.
Wenn der Partner oder die Partnerin keine nahe Beziehung erlaubt, zwischen dem Partner und seinem Partner gleichen Geschlechts.

Manchmal kann es ohne biologischen Grund sein.
Es kann so sein dass jemand über den eigenen Weg lief, der einfach ein herrlicher Mensch war.
Der genau gepasst hat, und dieser Mensch hätte jegliches Geschlecht haben können, aber er wurde zum einzigen, mit dem man das Leben teilen wollte.
Manchmal liegt es daran, dass jemand feststeckt in einer Symbiose, in einer Symbiose mit dem anderen Geschlecht.
Und da wird es nicht erlaubt, sexuelle Gefühle für dieses Geschlecht zu haben.
Das ist besetzt von etwas anderem, was dem im Weg steht.
Und deshalb löst die Person dies mit gleichgeschlechtlichen Beziehungen stattdessen.

<div class="handwritten">
 Warum ist die Homosexualität so lange Zeit verboten und gesetzeswidrig gewesen und ist so lange Zeit bestraft worden.
</div>

Das hat zu tun mit den Gesetzen die auf das Verurteilen und auf die Untreue gegenüber dem einen, dem einzigen allmächtigen Gott bauen, zu tun.
Das, genauso wie Selbstbefriedigung.
Die Männer dürften nicht mit den Händen unterhalb der Decke schlafen, die Männer dürften nicht schlafen mit Tieren.
Es war mit Todesstrafe belegt bis 1944, als es abgeschafft wurde.
Dann wurde es erlaubt, dann wurde wieder ein Gesetz erlassen, 2014, weil man es als Tierquälerei ansah, die Homosexualisierung wurde 1944 entkriminalisiert, aber sie blieb bestehen als psychische Störung bis 1979.
Und immer noch wird es von vielen als pervers angesehen.
Immer mehr sind es menschliche Codes und Normen, Totem und Tabu geworden, die den Trieb Sexualität regeln.
Und das Wichtige ist das Einverständnis.
Und das heißt, dass niemand dem anderen schadet in sexuellen Spielen.
Erst wenn einem Menschen Schaden zugefügt wird, wird die Handlung angeschaut, die zum Schaden geführt hat.
Aber dann wird dies in Zusammenhang gebracht mit dem was diese Gemeinheit oder diese Bösartigkeit bei dem Täter ausgelöst hat.
Und schädliche Handlungen uns selber gegenüber oder einander oder anderen gegenüber oder geg enüber Tieren sind gesetzeswidrig und werden beurteilt nach schwedischem Gesetz.
